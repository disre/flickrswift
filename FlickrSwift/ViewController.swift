//
//  ViewController.swift
//  FlickrSwift
//
//  Created by Kelson Vella on 10/18/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit

struct result: Decodable {
    let photos : page?
//    let stat: String
}

struct page: Decodable {
    let photo : [URLs]
//    let page: Int
//    let pages: Int
//    let perpage: Int
//    let total: String
}

struct URLs: Decodable {
    let id : String
    let owner: String
    let secret: String
    let server: String
    let farm: Int
    let title: String
}

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    var results:[URLs]?
    @IBOutlet var collection: UICollectionView!
    @IBOutlet var search: UISearchBar!
    
    @IBAction func newSearch(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        search.text = ""
        search.isHidden = false
        logo.isHidden = false
        self.results = nil
        self.collection.reloadData()
    }
    
    @IBOutlet var logo: UIImageView!
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if results != nil {
            return results!.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! flickrCell
        //clean up possible old stuff
        cell.image.image = nil
        cell.text.text = nil
        let photo = results![indexPath.item]
        DispatchQueue.global().async {
            let url = URL(string: "https://farm\(photo.farm).staticflickr.com/\(photo.server)/\(photo.id)_\(photo.secret).jpg")
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                
                cell.image.image = UIImage(data: data!)
            }
        }
        
        cell.text.text = photo.title
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/2 - 1, height: self.view.frame.size.width/2 - 1)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        search.delegate = self
        self.collection.register(UINib(nibName: "flickrCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view.backgroundColor = UIColor.darkGray
        
        UIApplication.shared.isStatusBarHidden = true
    }

//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
//        UIApplication.shared.isStatusBarHidden = true
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    fileprivate func flickrSearch(_ search:String) -> URL? {
        guard let term = search.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(term)&per_page=25&format=json&nojsoncallback=1"
        
        guard let url = URL(string:URLString) else {
            return nil
        }
        
        return url
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let url = flickrSearch(search.text!)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
//            let json = try! JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String, Any>
//            print(json)
            let json = try! JSONDecoder().decode(result.self, from: data)
            print(json)
            let pics = json.photos
            let urls = pics?.photo
            self.results = urls
            
            DispatchQueue.main.async {
                
                self.collection.reloadData()
            }
        }
        task.resume()
        logo.isHidden = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let cell = collectionView.cellForItem(at: indexPath) as! flickrCell
        if cell.image.image != nil {
            let picture = cell.image.image
            performSegue(withIdentifier: "viewImage", sender: picture)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextVC = segue.destination as! ImageViewController
        nextVC.photo = sender as? UIImage
    }
}

